import * as mongoose from 'mongoose';

const PlanSchema = new mongoose.Schema({
  name: { type: String, unique: true },
  minInvest: Number,
  maxInvest: Number,
  monthlyRate: Number,
  planDuration: Number,
  performanceDelivery: Number,
});

export { PlanSchema };
