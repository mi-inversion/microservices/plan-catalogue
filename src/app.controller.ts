import { Controller, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientProxy, MessagePattern } from '@nestjs/microservices';
import { PlanDto } from './dto/plan.dto';

const SERVICE_NAME = 'planCatalogue';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('SERVICES') private readonly client: ClientProxy,
  ) {}

  @MessagePattern({ service: SERVICE_NAME, cmd: 'createPlan' })
  async create(planDto: PlanDto) {
    const response = await this.appService.create(planDto);
    await this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'planCreated' },
        { request: planDto, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'updatePlan' })
  async update(planDto: PlanDto) {
    const response = await this.appService.update(planDto);
    await this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'planUpdated' },
        { request: planDto, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'deletePlan' })
  async delete(planDto: PlanDto) {
    const response = await this.appService.delete(planDto);
    await this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'planDeleted' },
        { request: planDto, response },
      )
      .toPromise();
    return response;
  }

  @MessagePattern({ service: SERVICE_NAME, cmd: 'getPlans' })
  async getAll() {
    const response = await this.appService.getAll();
    await this.client
      .emit(
        { service: SERVICE_NAME, cmd: 'plansConsulted' },
        { request: {}, response },
      )
      .toPromise();
    return response;
  }
}
