import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PlansRepository } from './repositories/plans.repository';
import { PlanDto } from './dto/plan.dto';

@Injectable()
export class AppService {
  constructor(private readonly planRepository: PlansRepository) {}

  async create(planDto: PlanDto) {
    return await this.planRepository.create(planDto);
  }
  async update(planDto: PlanDto) {
    return await this.planRepository.update(planDto);
  }
  async delete(planDto: PlanDto) {
    return await this.planRepository.delete(planDto);
  }
  async getAll() {
    return await this.planRepository.getAll();
  }
}
